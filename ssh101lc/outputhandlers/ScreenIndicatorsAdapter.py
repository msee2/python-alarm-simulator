#
# Software Verification and Validation course
# Master in Electronics Engineering - 2021
# Author: Luis G. Leon Vega <luis@luisleon.me>
#

from ssh101lc.outputhandlers.outputhandler import  OutputHandler
from ssh101lc.outputhandlers.outputenums import OutputType,OutputMode,OutputPayload

class ScreenIndicatorsAdapter(OutputHandler):
    """
    Sensor class for triggering the sensors

    Methods
    ----------
    start()
        Abstract. Starts listening to new events
    stop()
        Abstract. Stops listening to events and destroy all the resources
    trigger(payload)
        Trigger the callback
    """

    def __init__(self):
        """
        Constructs a new Screen + Indicators
        """
        self.__state = False
        self.NeedUpdate = False
        self.BATTERY_LED = 0
        self.ARMED_LED = 0
        self.Main_Display = ""
        self.Mode0_Display = ""
        self.Mode1_Display = ""
        self.Battery_Display = ""
        self.Error_Display = ""
        super().__init__()

    def start(self):
        """
        Starts the screen
        """
        self.__state = True
        self.NeedUpdate = False
        self.Main_Display = "888888888888888"
        self.BATTERY_LED = 0
        self.ARMED_LED = 0

    def stop(self):
        """
        Stops the Screen
        """
        self.__state = False
        self.Main_Display = ""
        self.BATTERY_LED = 0
        self.ARMED_LED = 0

    def show(self, message):
        if self.__state:
            self.NeedUpdate = True
            if ((message['mode'] == OutputMode.SHOW) & (message['type'] == OutputType.SCREEN)):                ##Print to screen
                self.Main_Display = self.Main_Display + message['payload']
                return

            if ((message['mode'] == OutputMode.CLEAR) & (message['type'] == OutputType.SCREEN)):              ## Clear screen
                self.Main_Display = ""
                return


            if ((message['mode'] == OutputMode.SHOW) & (message['type'] == OutputType.INDICATOR) & (message['payload'] == OutputPayload.MODE_0)):     #Show MODE_0
                self.Mode0_Display = "MODE_0"
                return

            if ((message['mode'] == OutputMode.CLEAR) & (message['type'] == OutputType.INDICATOR) & (message['payload'] == OutputPayload.MODE_0)):     #Clear MODE_0
                self.Mode0_Display = ""
                return

            if ((message['mode'] == OutputMode.SHOW) & (message['type'] == OutputType.INDICATOR) & (message['payload'] == OutputPayload.MODE_1)):     #Show MODE_1
                self.Mode1_Display = "MODE_1"
                return

            if ((message['mode'] == OutputMode.CLEAR) & (message['type'] == OutputType.INDICATOR) & (message['payload'] == OutputPayload.MODE_1)):     #Clear MODE_1
                self.Mode1_Display = ""
                return

            if ((message['mode'] == OutputMode.SHOW) & (message['type'] == OutputType.INDICATOR) & (message['payload'] == OutputPayload.ERROR)):     #Show MODE_1
                self.Error_Display = "ERROR"
                return

            if ((message['mode'] == OutputMode.CLEAR) & (message['type'] == OutputType.INDICATOR) & (message['payload'] == OutputPayload.ERROR)):     #Clear MODE_1
                self.Error_Display = ""
                return

            if ((message['mode'] == OutputMode.SHOW) & (message['type'] == OutputType.INDICATOR) & (message['payload'] == OutputPayload.BATTERY_LABEL)):     #Show MODE_1
                self.Battery_Display = "BATTERY"
                return

            if ((message['mode'] == OutputMode.CLEAR) & (message['type'] == OutputType.INDICATOR) & (message['payload'] == OutputPayload.BATTERY_LABEL)):     #Clear MODE_1
                self.Battery_Display = ""
                return



            if ((message['mode'] == OutputMode.SHOW) & (message['type'] == OutputType.INDICATOR) & (message['payload'] == OutputPayload.BATTERY)):  # Battery LED indicator ON
                self.BATTERY_LED = 1
                return

            if ((message['mode'] == OutputMode.CLEAR) & (message['type'] == OutputType.INDICATOR) & (message['payload'] == OutputPayload.BATTERY)):  # Battery LED indicator OFF
                self.BATTERY_LED = 0
                return

            if ((message['mode'] == OutputMode.BLINK) & (message['type'] == OutputType.INDICATOR) & (message['payload'] == OutputPayload.BATTERY)):  # Armed LED indicator BLINK
                self.BATTERY_LED = 2
                return

            if ((message['mode'] == OutputMode.SHOW) & (message['type'] == OutputType.INDICATOR) & (message['payload'] == OutputPayload.ARMED)):  # Armed LED indicator ON
                self.ARMED_LED = 1
                return

            if ((message['mode'] == OutputMode.CLEAR) & (message['type'] == OutputType.INDICATOR) & (message['payload'] == OutputPayload.ARMED)):  # Armed LED indicator OFF
                self.ARMED_LED = 0
                return

            if ((message['mode'] == OutputMode.BLINK) & (message['type'] == OutputType.INDICATOR) & (message['payload'] == OutputPayload.ARMED)):  # Armed LED indicator BLINK
                self.ARMED_LED = 2
                return
