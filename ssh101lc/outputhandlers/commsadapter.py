#
# Software Verification and Validation course
# Master in Electronics Engineering - 2021
# Author: Luis G. Leon Vega <luis@luisleon.me>
#

import sys

from ssh101lc.outputhandlers.outputhandler import OutputHandler
from ssh101lc.outputhandlers.outputenums import OutputType


class CommsAdapter(OutputHandler):
    """
    Interface class for abstracting and standardising the output modules

    Methods
    ----------
    start()
        Abstract. Prepares the resources to be used
    stop()
        Abstract. Releases the resources already reserved by start
    show()
        Abstract. 
    """

    def __init__(self):
        pass

    def start(self):
        """
        Starts the input listener. This will deploy threads and other
        structures required for polling or handling the events.
        """
        pass

    def stop(self):
        """
        Stops the input listener. This will destroy threads and other
        structures required for polling or handling the events.
        """
        pass

    def show(self, message):
        """
        Flushes a message to the output resource. If the resource is a screen
        or an indicator, this method shows the message on the screen or toggles
        the indicator
        """
        if message["type"] != OutputType.COMMS:
            return

        message = "Comms<{number}>: u{usercode}|s{sensorcode}|t{type}".format(
            number=message["payload"].get("number", "nonumber"),
            usercode=message["payload"].get("user_code", "nouser"),
            sensorcode=message["payload"].get("sensor_code", "-1"),
            type=message["payload"].get("alarm_type", "-1")
        )

        sys.stderr.write(message)
        sys.stderr.flush()

    def __del__(self):
        """
        Destructor
        """
        self.stop()
