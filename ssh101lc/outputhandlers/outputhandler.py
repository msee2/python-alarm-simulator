#
# Software Verification and Validation course
# Master in Electronics Engineering - 2021
# Author: Luis G. Leon Vega <luis@luisleon.me>
#

import abc


class OutputHandler(abc.ABC):
    """
    Interface class for abstracting and standardising the output modules

    Methods
    ----------
    start()
        Abstract. Prepares the resources to be used
    stop()
        Abstract. Releases the resources already reserved by start
    show()
        Abstract. 
    """




    @abc.abstractmethod
    def start(self):
        """
        Starts the input listener. This will deploy threads and other
        structures required for polling or handling the events.
        """
        pass

    @abc.abstractmethod
    def stop(self):
        """
        Stops the input listener. This will destroy threads and other
        structures required for polling or handling the events.
        """
        pass

    @abc.abstractmethod
    def show(self, message):
        """
        Flushes a message to the output resource. If the resource is a screen
        or an indicator, this method shows the message on the screen or toggles
        the indicator
        """
        pass

    def __del__(self):
        """
        Destructor
        """
        self.stop()
