#
# Software Verification and Validation course
# Master in Electronics Engineering - 2021
# Author: Luis G. Leon Vega <luis@luisleon.me>
#

from pydub import AudioSegment
from pydub.playback import play
from threading import Thread, Lock
from os import path

from ssh101lc.outputhandlers.outputhandler import OutputHandler
from ssh101lc.outputhandlers.outputenums import OutputType


class HornAdapter(OutputHandler):
    """
    Interface class for abstracting and standardising the output modules

    Methods
    ----------
    start()
        Abstract. Prepares the resources to be used
    stop()
        Abstract. Releases the resources already reserved by start
    show()
        Abstract. 
    """

    def __init__(self):
        self.__activate = False
        self.__lock = Lock()
        self.__thread = None
        file = path.join(path.dirname(__file__), "res\\siren.wav")
        self.__audio = AudioSegment.from_wav(file)

    def start(self):
        """
        Starts the input listener. This will deploy threads and other
        structures required for polling or handling the events.
        """
        pass

    def stop(self):
        """
        Stops the input listener. This will destroy threads and other
        structures required for polling or handling the events.
        """
        with self.__lock:
            self.__activate = False
        if self.__thread:
            self.__thread.join()
        self.__thread = None

    def __reproduce_sound(self):
        while(True):
            with self.__lock:
                if not self.__activate:
                    break
            play(self.__audio)

    def show(self, message):
        """
        Flushes a message to the output resource. If the resource is a screen
        or an indicator, this method shows the message on the screen or toggles
        the indicator
        """
        if message["type"] != OutputType.HORN:
            return

        self.__activate = True
        self.__thread = Thread(target=self.__reproduce_sound)
        self.__thread.start()

    def set_sound(self, type=0):
        # Type 0 is for horn
        if type == 0:
            file = path.join(path.dirname(__file__), "res\\siren.wav")
            print(file)
            self.__audio = AudioSegment.from_wav(file)
        else:
            file = path.join(path.dirname(__file__), "res\\beep.wav")
            self.__audio = AudioSegment.from_wav(file)

    def __del__(self):
        """
        Destructor
        """
        self.stop()
