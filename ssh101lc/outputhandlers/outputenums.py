#
# Software Verification and Validation course
# Master in Electronics Engineering - 2021
# Author: Luis G. Leon Vega <luis@luisleon.me>
#

from enum import Enum


class OutputType(Enum):
    SCREEN = 0
    INDICATOR = 1
    HORN = 2
    COMMS = 3

class OutputMode(Enum):
    SHOW = 0
    CLEAR = 1
    BLINK = 2

class OutputPayload(Enum):
    BATTERY = 0
    ARMED = 1
    ERROR = 2
    MODE_0 = 3
    MODE_1 = 4
    BATTERY_LABEL = 5

class AlarmType(Enum):
    THEFT = 0
    PANIC = 1
    FIRE = 2
