import time
import tkinter
from tkinter import Button, Frame, Tk, Label, Checkbutton  # Python 2
from ssh101lc.inputhandlers.sensoradapter import SensorAdapter
from threading import Thread, Lock
from ssh101lc.inputhandlers.inputenums import InputMode,InputType,InputPayload



class GUI:
    def __init__(self, master, sensor, button, screen):

        # Instantiating master
        # i.e toplevel Widget
        self.master = master
        self.__sensor = sensor
        self.__button = button
        self.__screen = screen
        self.comando = ""


        self.__Armedactivate = False
        self.__Armedlock = Lock()
        self.__Armedthread = None

        self.__Batteryactivate = False
        self.__Batterylock = Lock()
        self.__Batterythread = None


        master.title("SSH-101")
        master.geometry('410x250')  # normal size
        master.maxsize(width=480, height=270)  # maximum size
        master.minsize(width=480, height=270)  # minimum size
        # end window size

        master.configure(bg='light blue')  # add background color

        #master.bind_class('<Button-1>', self.button_pressed())
        #master.bind_class('<ButtonRelease>', self.button_released())


        self.button1 = Button(self.master, text="1", command=self.button_1_click)
        self.button1.place(x=250, y=10)


        self.button2 = Button(self.master, text="2", command=self.button_2_click)
        self.button2.place(x=280, y=10)
        self.button3 = Button(self.master, text="3", command=self.button_3_click)
        self.button3.place(x=310, y=10)
        self.button4 = Button(self.master, text="4", command=self.button_4_click)
        self.button4.place(x=250, y=40)
        self.button5 = Button(self.master, text="5", command=self.button_5_click)
        self.button5.place(x=280, y=40)
        self.button6 = Button(self.master, text="6", command=self.button_6_click)
        self.button6.place(x=310, y=40)
        self.button7 = Button(self.master, text="7", command=self.button_7_click)
        self.button7.place(x=250, y=70)
        self.button8 = Button(self.master, text="8", command=self.button_8_click)
        self.button8.place(x=280, y=70)
        self.button9 = Button(self.master, text="9", command=self.button_9_click)
        self.button9.place(x=310, y=70)
        self.button0 = Button(self.master, text="0", command=self.button_0_click)
        self.button0.place(x=280, y=100)
        self.button_asterisk = Button(self.master, text="*", command=self.button_asterisk_click)
        self.button_asterisk.place(x=250, y=100)
        self.button_hash = Button(self.master, text="#", command=self.button_hash_click)
        self.button_hash.place(x=310, y=100)
        self.button_escape = Button(self.master, text="   ESC  ", command=self.button_escape_click)
        self.button_escape.place(x=340, y=10)
        self.button_enter = Button(self.master, text="  ENTER ", command=self.button_enter_click)
        self.button_enter.place(x=340, y=40)
        self.button_panico = Button(self.master, text=" PANICO + 3S", command=self.button_panico_click)
        self.button_panico.place(x=340, y=70)
        self.button_bomberos = Button(self.master, text="BOMBEROS + 3S", command=self.button_bomberos_click)
        self.button_bomberos.place(x=340, y=100)

        self.button_EnterPlusAsterisk = Button(self.master, text="ENTER + *", command=self.button_EnterPlusAsterisk_click)
        self.button_EnterPlusAsterisk.place(x=340, y=140)

        self.button_EnterPlusHash = Button(self.master, text="ENTER + #", command=self.button_EnterPlusHash_click)
        self.button_EnterPlusHash.place(x=340, y=170)

        self.button_EnterPlusEscape = Button(self.master, text="ENTER + ESC", command=self.button_EnterPlusEscape_click)
        self.button_EnterPlusEscape.place(x=340, y=200)

        self.button_Enter_3sec = Button(self.master, text="ENTER 3_SEC", command=self.button_enter_3sec_click)
        self.button_Enter_3sec.place(x=340, y=230)

        self.display = Label(self.master, text="888888888888888", width=15, height=5, font=24)
        self.display.place(x=10, y=10)

        self.display_modo0 = Label(self.master, text="", width=7, height=2)
        self.display_modo0.place(x=180, y=10)

        self.display_modo1 = Label(self.master, text="", width=7, height=2)
        self.display_modo1.place(x=180, y=40)

        self.display_bateria = Label(self.master, text="", width=7, height=2)
        self.display_bateria.place(x=180, y=70)

        self.display_error = Label(self.master, text="ERROR", width=7, height=2)
        self.display_error.place(x=180, y=95)

        self.armado = Checkbutton(self.master, text="ARMADA",  height=1, width=7)

        self.armado.place(x=120, y=150)
        self.armado.deselect()

        self.bateria = Checkbutton(self.master, text="BATERIA", height=1, width=6)

        self.bateria.place(x=10, y=150)
        self.bateria.deselect()


    def button_pressed(event):
        print('Button pressed.')

    def button_released(event):
        print('Button Released.')


    def button_1_click(self) -> None:

        callback_message = {
            "type": InputType.KEYBOARD,
            "mode": InputMode.TYPED,
            "payload": "1"
        }
        self.__button.trigger(callback_message)

        self.Update_Gui()

    def button_2_click(self) -> None:
        callback_message = {
            "type": InputType.KEYBOARD,
            "mode": InputMode.TYPED,
            "payload": "2"
        }
        self.__button.trigger(callback_message)
        self.display.config(text=self.__screen.Main_Display)

    def button_3_click(self) -> None:
        callback_message = {
            "type": InputType.KEYBOARD,
            "mode": InputMode.TYPED,
            "payload": "3"
        }
        self.__button.trigger(callback_message)

        self.display.config(text=self.__screen.Main_Display)

    def button_4_click(self) -> None:
        callback_message = {
            "type": InputType.KEYBOARD,
            "mode": InputMode.TYPED,
            "payload": "4"
        }
        self.__button.trigger(callback_message)
        self.display.config(text=self.__screen.Main_Display)

    def button_5_click(self) -> None:
        callback_message = {
            "type": InputType.KEYBOARD,
            "mode": InputMode.TYPED,
            "payload": "5"
        }
        self.__button.trigger(callback_message)

        self.display.config(text=self.__screen.Main_Display)

    def button_6_click(self) -> None:
        callback_message = {
            "type": InputType.KEYBOARD,
            "mode": InputMode.TYPED,
            "payload": "6"
        }
        self.__button.trigger(callback_message)

        self.display.config(text=self.__screen.Main_Display)

    def button_7_click(self) -> None:
        callback_message = {
            "type": InputType.KEYBOARD,
            "mode": InputMode.TYPED,
            "payload": "7"
        }
        self.__button.trigger(callback_message)

        self.display.config(text=self.__screen.Main_Display)

    def button_8_click(self) -> None:
        callback_message = {
            "type": InputType.KEYBOARD,
            "mode": InputMode.TYPED,
            "payload": "8"
        }
        self.__button.trigger(callback_message)

        self.display.config(text=self.__screen.Main_Display)

    def button_9_click(self) -> None:
        callback_message = {
            "type": InputType.KEYBOARD,
            "mode": InputMode.TYPED,
            "payload": "9"
        }
        self.__button.trigger(callback_message)

        self.display.config(text=self.__screen.Main_Display)

    def button_0_click(self) -> None:
        callback_message = {
            "type": InputType.KEYBOARD,
            "mode": InputMode.TYPED,
            "payload": "0"
        }
        self.__button.trigger(callback_message)

        self.display.config(text=self.__screen.Main_Display)

    def button_asterisk_click(self) -> None:
        callback_message = {
            "type": InputType.KEYBOARD,
            "mode": InputMode.TYPED,
            "payload": "10"
        }
        self.__button.trigger(callback_message)

        self.display.config(text=self.__screen.Main_Display)

    def button_hash_click(self) -> None:
        callback_message = {
            "type": InputType.KEYBOARD,
            "mode": InputMode.TYPED,
            "payload": "11"
        }
        self.__button.trigger(callback_message)
        self.Update_Gui()

    def button_escape_click(self) -> None:
        callback_message = {
            "type": InputType.KEYBOARD,
            "mode": InputMode.TYPED,
            "payload": "12"
        }
        self.__button.trigger(callback_message)
        self.Update_Gui()

    def button_enter_click(self) -> None:
       # start = time.process_time_ns()
       # stop = time.process_time_ns()
       # print(stop - start)
        callback_message = {
            "type": InputType.KEYBOARD,
            "mode": InputMode.TYPED,
            "payload": "13"
        }
        self.__button.trigger(callback_message)
        self.Update_Gui()

    def button_enter_3sec_click(self) -> None:
        callback_message = {
            "type": InputType.KEYBOARD,
            "mode": InputMode.HOLD_3SEC,
            "payload": "13"
        }
        self.__button.trigger(callback_message)
        self.Update_Gui()

    def button_panico_click(self) -> None:
        callback_message = {
            "type": InputType.KEYBOARD,
            "mode": InputMode.HOLD_3SEC,
            "payload": "14"
        }
        self.__button.trigger(callback_message)

    def button_bomberos_click(self) -> None:
        callback_message = {
            "type": InputType.KEYBOARD,
            "mode": InputMode.HOLD_3SEC,
            "payload": "15"
        }
        self.__button.trigger(callback_message)

    def button_EnterPlusAsterisk_click(self) -> None:
        callback_message = {  # callback_message = callback message
            "type": InputType.KEYBOARD,
            "mode": InputMode.HOLD_3SEC,
            "payload": "16"
        }
        self.__button.trigger(callback_message)
        self.Update_Gui()

    def button_EnterPlusHash_click(self) -> None:
        callback_message = {  # callback_message = callback message
            "type": InputType.KEYBOARD,
            "mode": InputMode.HOLD_3SEC,
            "payload": "17"
        }
        self.__button.trigger(callback_message)
        self.Update_Gui()

    def button_EnterPlusEscape_click(self) -> None:
        callback_message = {  # callback_message = callback message
            "type": InputType.KEYBOARD,
            "mode": InputMode.HOLD_3SEC,
            "payload": "18"
        }
        self.__button.trigger(callback_message)
        self.Update_Gui()


    def LED_indicators(self,ARMED_LED,BATTERY_LED):

        if (BATTERY_LED == 0):
            self.Stop_Battery_Blink_LED()
            self.bateria.deselect()

        if (BATTERY_LED == 1 ):
            self.Stop_Battery_Blink_LED()
            self.bateria.select()


        if (BATTERY_LED == 2):
            if self.__Batteryactivate:
                return
            with self.__Batterylock:
                self.__Batteryactivate = True
            self.__Batterythread = Thread(target=self.Start_Battery_Blink_LED)
            self.__Batterythread.start()

        if (ARMED_LED == 0):
            self.Stop_Armed_Blink_LED()
            self.armado.deselect()

        if (ARMED_LED == 1):
            self.Stop_Armed_Blink_LED()
            self.armado.select()

        if (ARMED_LED == 2):
            if self.__Armedactivate:
                return
            with self.__Armedlock:
                self.__Armedactivate = True
            self.__Armedthread = Thread(target=self.Start_Armed_Blink_LED)
            self.__Armedthread.start()


    def Update_Gui(self):
        if self.__screen.NeedUpdate:
            self.display.config(text=self.__screen.Main_Display)
            self.LED_indicators(self.__screen.ARMED_LED, self.__screen.BATTERY_LED)
            self.display_modo0.config(text=self.__screen.Mode0_Display)
            self.display_modo1.config(text=self.__screen.Mode1_Display)
            self.display_error.config(text=self.__screen.Error_Display)
            self.display_bateria.config(text=self.__screen.Battery_Display)
            self.__screen.NeedUpdate = False


    def Stop_Armed_Blink_LED(self):
        with self.__Armedlock:
            self.__Armedactivate = False
        if self.__Armedthread:
            self.__Armedthread.join()
        self.__Armedthread = None

    def Stop_Battery_Blink_LED(self):
        with self.__Batterylock:
            self.__Batteryactivate = False
        if self.__Batterythread:
            self.__Batterythread.join()
        self.__Batterythread = None

    def Start_Armed_Blink_LED(self):
        i = 0
        while(True):
            with self.__Armedlock:
                if not self.__Armedactivate:
                    break
            if i % 6_000_000 == 0:
                self.armado.select()
            elif i % 3_000_000 == 0:
                self.armado.deselect()
            i = i + 1


    def Start_Battery_Blink_LED(self):
        i = 0
        while(True):
            with self.__Batterylock:
                if not self.__Batteryactivate:
                    break
            if i % 6_000_000 == 0:
                self.bateria.select()
            elif i % 3_000_000 == 0:
                self.bateria.deselect()
            i = i + 1

    def stop(self):
        self.Stop_Battery_Blink_LED()
        self.Stop_Armed_Blink_LED()
