#
# Software Verification and Validation course
# Master in Electronics Engineering - 2021
# Author: Luis G. Leon Vega <luis@luisleon.me>
#

from threading import Thread


class CLIConnector:
    def __init__(self):
        self.__sensor_callback = None
        self.__battery_callback = None
        self.__thread = None
        self.__terminate = False

    def attach_sensor_callback(self, cb):
        if callable(cb) or cb is None:
            self.__sensor_callback = cb
        else:
            print("Error: sensor callback is not callable")

    def attach_battery_callback(self, cb):
        if callable(cb) or cb is None:
            self.__battery_callback = cb
        else:
            print("Error: battery callback is not callable")

    def __print(self):
        message = """
        Presione q para salir
        Active un sensor escribiendo S[Numero].
            Ejemplo: S0 activa sensor 0
        Actualice el valor de la bateria escribiendo B[valor]. Valor máximo = 12.0
            Ejemplo: B9.6 indica 9.6 Volts
        Para enviar, presione Enter
        """
        print(message)

    def __process_message(self, message):
        if message == "q" or message == "Q":
            self.__terminate = True
        elif message[0] == "B":
            try:
                volt = float(message[1:])
                if self.__battery_callback and volt <= 12.0:
                    self.__battery_callback(volt)
                else:
                    print('Battery max input = 12.0. Enter value again')
            except Exception as exception:
                print("Error: ", exception)
        elif message[0] == "S":
            try:
                sensor = int(message[1:])
                if self.__sensor_callback:
                    self.__sensor_callback(sensor)
            except Exception as exception:
                print("Error: ", exception)

    def __worker(self):
        while(not self.__terminate):
            self.__print()
            incoming = input()
            self.__process_message(incoming)

    def start(self):
        self.__terminate = False
        if not self.__thread:
            self.__thread = Thread(target=self.__worker)
            self.__thread.start()

    def stop(self):
        if not self.__terminate:
            self.__terminate = True
            self.__thread.join()
            self.__thread = None

    def __del__(self):
        self.stop()
