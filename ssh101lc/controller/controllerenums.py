from enum import Enum


class ControllerState(Enum):
    Unprogrammed = 0
    UserNumberConf = 1
    SecurityPhoneNumberConf = 2
    ArmingCodeConf = 3
    ZoneProgramming = 4
    Programmed = 5    # In this state the alarm contains all codes and sensors's
    ArmingMode_0 = 6
    ArmingMode_1 = 7
    PanicMode = 8
    FireMode = 9
    Mode_0_Triggered = 10
    Mode_1_Triggered = 11
    Low_Battery = 12

    UserNumberPreConf = 13
    SecurityNumberPreConf = 14
    ArmingCodeConfMod0 = 15
    ArmingCodeConfMod1 = 16
    


    # states are 0 = unprogrammed, 1= configuracion # de usuario en proceso, 2= conf usuario finalizada
    # compania seguridad
    # 3= configurar codigo armado , 4 = programacion de zonas, 5 = armado modo 0, 6= armado modo 1
    # 7 = disparado modo 0, 8 = disparado modo 1, 9 = modo Panico, 10 = modo Incendio
