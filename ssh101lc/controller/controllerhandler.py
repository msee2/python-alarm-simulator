#
# Software Verification and Validation course
# Master in Electronics Engineering - 2021
# Author: Luis G. Leon Vega <luis@luisleon.me>
#

import abc


class ControllerHandler(abc.ABC):

    def __init__(self, input_handlers={}, output_handlers={}):
        self.input_handlers = input_handlers
        self.output_handlers = output_handlers



    @abc.abstractmethod
    def init(self):
        """
        Initialises the objects
        """
        pass

    def start(self):
        """
        Starts the controller and reserve all the resources
        """
        for i in self.input_handlers:
            self.input_handlers[i].start()

        for i in self.output_handlers:
            self.output_handlers[i].start()

    @abc.abstractmethod
    def reset(self):
        """
        Restarts the controller
        """
        pass

    def stop(self):
        """
        Stops the controller and reserve all the resources
        """
        for i in self.input_handlers:
            self.input_handlers[i].stop()

        for i in self.output_handlers:
            self.output_handlers[i].stop()
