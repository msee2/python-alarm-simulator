import time
import threading

from ssh101lc.controller.controllerenums import ControllerState
from ssh101lc.logger.logger import get_logger
from ssh101lc.inputhandlers.inputenums import InputPayload, InputMode
from ssh101lc.outputhandlers.outputenums import AlarmType


class StatesLogic:
    def __init__(self, controller, logger=None):
        self.__controller = controller
        self.__buffer = ""
        self._last_char = ""
        self.__controller.state = ControllerState.Unprogrammed
        if not logger:
            self.__logger = get_logger("State Logic")
        self.__cleaner_counter = 0
        self.__cleaner_terminate = False
        self.__cleaner_thread = None
        self.__thread = None
        self._lock = threading.Lock()
        self.__thread_argv = {}

    @property
    def _buffer(self):
        with self._lock:
            return self.__buffer
    
    @_buffer.setter
    def _buffer(self, val):
        with self._lock:
            self.__buffer = val

    def __cleaner_worker(self):
        do_work = True
        do_clean = False
        
        while do_work:
            # Wait for seconds
            time.sleep(1)
            state = self.__controller.state
            # Terminate condition
            with self._lock:
                do_clean = self.__cleaner_counter >= 10
                if do_clean:
                    self.__cleaner_counter = 0
                do_work = not self.__cleaner_terminate
                self.__cleaner_counter += 1
            if do_clean:
                if len(self._buffer) > 0 and state in [ControllerState.UserNumberConf,ControllerState.SecurityPhoneNumberConf,
                                                           ControllerState.ArmingCodeConf,ControllerState.ZoneProgramming,
                                                           ControllerState.UserNumberPreConf,ControllerState.SecurityNumberPreConf,
                                                           ControllerState.ArmingCodeConfMod0,ControllerState.ArmingCodeConfMod1]:
                    self.configuration_clear()
                    self.configuration_check()


    def to_unprogrammed(self):
        self.__controller.state = ControllerState.Unprogrammed
        self.__controller.clear_screen()
        self.__controller.show_screen({
            "payload": "8888888888"
        })
        self.__controller.Battery_LED_Off()
        self.__controller.Armed_LED_Off()
        self.__controller.show_Mode0_Indicator(None)
        self.__controller.show_Mode1_Indicator(None)
        self.__controller.show_Error_Indicator(None)

    def to_user_number_conf(self):
        self.__controller.state = ControllerState.UserNumberPreConf
        self.__controller.Battery_LED_On()
        self.__controller.Armed_LED_Blink()
        self.__controller.clear_screen()

    def configuration_clear(self):
        self._buffer = ""
        self.__controller.clear_screen()

    def user_number_conf_show_pass(self):
        if len(self._buffer) > 4:
            self.configuration_clear()

        self.__controller.show_screen({
            "payload": self._last_char
        })

    def check_user_code(self):
        return self._buffer == self.__controller.user_code

    def user_number_conf_check_def_pass(self):
        if self.check_user_code():
            self.__controller.clear_screen()
            self.__controller.Battery_LED_On()
            self.__controller.Armed_LED_On()
            time.sleep(1)
            self.__controller.Battery_LED_On()
            self.__controller.Armed_LED_Blink()
            self.__controller.state = ControllerState.UserNumberConf

        self.configuration_clear()

    def to_programmed(self):
        self.__controller.state = ControllerState.Programmed
        self.__controller.Battery_LED_Off()
        self.__controller.Armed_LED_Off()
        self.__controller.clear_Mode0_Indicator(None)
        self.__controller.clear_Mode1_Indicator(None)
        self.__controller.clear_Error_Indicator(None)
        self.__controller.clear_screen()
        self._buffer = ""

    def type_buffer(self, val):
        self._buffer += val
        self._last_char = val
        with self._lock:
            self.__cleaner_counter = 0

    def configuration_check(self):
        programmed = \
            self.__controller.configured.get("UserCode", False) and \
            self.__controller.configured.get("SecurityNumber", False) and \
            self.__controller.configured.get("ArmingCode0", False) and \
            self.__controller.configured.get("ArmingCode1", False)
        if not programmed:
            self.to_unprogrammed()
        else:
            self.to_programmed()

    def user_number_save(self):
        if len(self._buffer) != 4:
            self.configuration_clear()
        else:
            self.__controller.user_code = self._buffer
            self.__controller.configured["UserCode"] = True
            self.configuration_check()

    def to_security_number_pre_conf(self):
        self.__controller.state = ControllerState.SecurityNumberPreConf
        self.configuration_clear()
        self.__controller.Battery_LED_Blink()
        self.__controller.Armed_LED_On()

    def security_number_conf_check_def_pass(self):
        if self.check_user_code():
            self.__controller.state = ControllerState.SecurityPhoneNumberConf
            self.__controller.Battery_LED_On()
            self.__controller.Armed_LED_On()
            time.sleep(1)
            self.__controller.Battery_LED_Blink()
            self.__controller.Armed_LED_On()

        self.configuration_clear()

    def security_number_conf_show_number(self):
        if len(self._buffer) > 8:
            self.configuration_clear()

        self.__controller.show_screen({
            "payload": self._last_char
        })

    def security_number_save(self):
        if len(self._buffer) != 8:
            self.configuration_clear()
        else:
            self.__controller.security_number = self._buffer
            self.__controller.configured["SecurityNumber"] = True
            self.configuration_check()

    def to_armed_pre_conf(self):
        self.__controller.state = ControllerState.ArmingCodeConf
        self.configuration_clear()
        self.__controller.Battery_LED_Blink()
        self.__controller.Armed_LED_Blink()

    def arming_conf_show_mode(self):
        if len(self._buffer) > 2:
            self.configuration_clear()

        self.__controller.show_screen({
            "payload": self._last_char
        })

    def arming_conf_set_mode(self):
        valid = False
        if self._buffer == "00":
            valid = True
            self.__controller.state = ControllerState.ArmingCodeConfMod0
        elif self._buffer == "01":
            valid = True
            self.__controller.state = ControllerState.ArmingCodeConfMod1

        if valid:
            self.__controller.clear_screen()
            self.__controller.Battery_LED_On()
            self.__controller.Armed_LED_On()
            time.sleep(1)
            self.__controller.Battery_LED_Blink()
            self.__controller.Armed_LED_On()

        self.configuration_clear()

    def arming_conf_show_code(self):
        if len(self._buffer) > 6:
            self.configuration_clear()

        self.__controller.show_screen({
            "payload": self._last_char
        })

    def arming_save_code(self):
        if len(self._buffer) != 6:
            self.configuration_clear()
        elif self.__controller.state == ControllerState.ArmingCodeConfMod0:
            self.__controller.arming_mode_0 = self._buffer
            self.__controller.configured["ArmingCode0"] = True
            self.configuration_check()
        elif self.__controller.state == ControllerState.ArmingCodeConfMod1:
            self.__controller.arming_mode_1 = self._buffer
            self.__controller.configured["ArmingCode1"] = True
            self.configuration_check()

    def to_programming_zones(self):
        self.__controller.state = ControllerState.ZoneProgramming
        # self.__controller.Battery_LED_Off()
        self.__controller.Armed_LED_Blink()
        self.configuration_clear()

    def programming_zones_show_numbers(self):
        if len(self._buffer) > 2:
            self.configuration_clear()

        self.__controller.show_screen({
            "payload": self._last_char
        })

    def programming_zones_save(self):
        if len(self._buffer) != 2:
            self.configuration_clear()
            self._buffer = ""
            return

        sensor_num = int(self._buffer)
        if sensor_num >= 0 and sensor_num < 10:
            if sensor_num not in self.__controller.Mode_1_Sensors_List:
                self.__controller.Mode_1_Sensors_List.append(sensor_num)
            else:
                self.__controller.Mode_1_Sensors_List.remove(sensor_num)
        self._buffer = ""
        self.configuration_clear()

    def programming_zones_finish(self):
        self.__controller.configured["Sensors1"] = True
        self.configuration_check()

    def to_arming_0(self):
        self.__controller.state = ControllerState.ArmingMode_0
        self.__controller.Armed_LED_On()
        self.__controller.show_Mode0_Indicator(None)
        self.configuration_clear()

    def to_arming_1(self):
        self.__controller.state = ControllerState.ArmingMode_1
        self.__controller.Armed_LED_On()
        self.__controller.show_Mode1_Indicator(None)
        self.configuration_clear()

    def out_arming_0(self):
        self.__controller.clear_Mode0_Indicator(None)
        self.configuration_check()

    def out_arming_1(self):
        self.__controller.clear_Mode1_Indicator(None)
        self.configuration_check()

    def programmed_show_numbers(self):
        if len(self._buffer) > 7:
            self.configuration_clear()
            self._buffer = ""
            return

        self.__controller.show_screen({
            "payload": self._last_char
        })

    def programmed_check_armed(self, mode):
        state = self.__controller.state
        passw = self._buffer[0:6]
        if mode == 0:
            if self.__controller.arming_mode_0 == passw:
                if state == ControllerState.Programmed:
                    self.to_arming_0()
                elif state in [ControllerState.ArmingMode_0, ControllerState.Mode_0_Triggered]:
                    self.to_programmed()
                    self.__controller.stop_beep()
        else:
            if self.__controller.arming_mode_1 == passw:
                if state == ControllerState.Programmed:
                    self.to_arming_1()
                elif state in [ControllerState.ArmingMode_1, ControllerState.Mode_1_Triggered]:
                    self.to_programmed()
                    self.__controller.stop_beep()

    def programmed_arm(self):
        if self._buffer[len(self._buffer) - 1] == "*":
            self.programmed_check_armed(0)
        elif self._buffer[len(self._buffer) - 1] == "#":
            self.programmed_check_armed(1)
        else:
            self.configuration_clear()
            self._buffer = ""

    def trigger_panic(self):
        self.__controller.state = ControllerState.PanicMode
        self.__controller.trigger_panic_buttons(AlarmType.PANIC)

    def trigger_fire(self):
        self.__controller.state = ControllerState.FireMode
        self.__controller.trigger_panic_buttons(AlarmType.FIRE)

    def trigger_armed0(self, sensor):
        self.__controller.state = ControllerState.Mode_0_Triggered
        self.__controller.trigger_alarm0(sensor)

    def __worker(self):
        with self._lock:
            sensor = self.__thread_argv.get("sensor")
            if self.__thread_argv.get("sensor") == 0:
                self.__controller.do_beep()
        time.sleep(30)
        with self._lock:
            if self.__controller.state not in [ControllerState.Programmed, ControllerState.Unprogrammed]:
                self.__controller.state = ControllerState.Mode_1_Triggered
                self.__controller.trigger_alarm0(sensor)
            else:
                self.__controller.stop_beep()

    def trigger_armed1(self, sensor):
        if sensor not in self.__controller.Mode_1_Sensors_List:
            return

        self.__controller.state = ControllerState.Mode_1_Triggered
        # Door is S0
        with self._lock:
            self.__thread_argv["sensor"] = sensor
        if self.__thread:
            self.__thread.join()
            self.__thread = None
        self.__thread = threading.Thread(target=self.__worker)
        self.__thread.start()

    def start(self):
        self.__cleaner_terminate = False
        self.__cleaner_thread = threading.Thread(target=self.__cleaner_worker)
        self.__cleaner_thread.start()
    
    def stop(self):
        with self._lock:
            self.__cleaner_terminate = True
        if self.__cleaner_thread:
            self.__cleaner_thread.join()
            self.__cleaner_thread = None
        if self.__thread:
            self.__thread.join()
            self.__thread = None

    def __del__(self):
        self.stop()


class StatesTransition:
    def __init__(self, controller, logger=None):
        self.__controller = controller
        self._states_logic = self.__controller._states_logic
        if not logger:
            self.__logger = get_logger("State Logic")

    def handle_keyboard_numbers(self, payload, mode):
        if int(payload) >= 0 and int(payload) <= 9:
            self._states_logic.type_buffer(payload)
            return True
        return False

    def handle_all_keyboard_chars(self, payload, mode):
        if self.handle_keyboard_numbers(payload, mode):
            return True
        elif int(payload) == 10:
            self._states_logic.type_buffer("*")
            return True
        elif int(payload) == 11:
            self._states_logic.type_buffer("#")
            return True
        return False

    def sensor_logic(self, payload, mode):
        state = self.__controller.state
        if state == ControllerState.ArmingMode_0 and mode == 0:
            self._states_logic.trigger_armed0(payload)
        elif state == ControllerState.ArmingMode_1 and mode == 0:
            self._states_logic.trigger_armed1(payload)

    def battery_logic(self, payload, mode):
        # TODO: Fix need for button
        state = self.__controller.state
        if state not in [ControllerState.Unprogrammed, ControllerState.Programmed, ControllerState.ArmingMode_0, ControllerState.ArmingMode_1]:
            return
        if payload > 8.64:
            self.__controller.Battery_LED_Off()
            self.__controller.clear_Battery_Indicator()
        else:
            self.__controller.show_Battery_Indicator()
            self.__controller.Battery_LED_On()


    def next_states_logic(self, payload, mode):
        state = self.__controller.state
        # TODO: Remove this later
        if payload == InputPayload.ENTER.value and state == ControllerState.Unprogrammed:
            if mode != InputMode.HOLD_3SEC:
                return self._states_logic.configuration_check

        # Transition back to the original
        if payload == InputPayload.ESCAPE.value and mode == InputMode.HOLD_3SEC:
            return self._states_logic.configuration_check

        # Any transition coming from Unprogrammed
        if state == ControllerState.Unprogrammed:
            if payload == InputPayload.ENTER_ASTERISK.value:
                return self._states_logic.to_user_number_conf
            elif payload == InputPayload.ENTER_HASH.value:
                return self._states_logic.to_security_number_pre_conf
            elif payload == InputPayload.ENTER_ESCAPE.value:
                return self._states_logic.to_armed_pre_conf
            elif payload == InputPayload.ENTER.value:
                if mode == InputMode.HOLD_3SEC:
                    return self._states_logic.configuration_check
                if mode == InputMode.TYPED:
                    return self._states_logic.configuration_check

        # Any transition coming from Programmed
        if state == ControllerState.Programmed:
            if self.handle_all_keyboard_chars(payload, mode):
                return self._states_logic.programmed_show_numbers
            elif payload == InputPayload.ESCAPE.value:
                return self._states_logic.user_number_conf_clear
            elif payload == InputPayload.ENTER.value:
                if mode == InputMode.HOLD_3SEC:
                    return self._states_logic.to_programming_zones
                if mode == InputMode.TYPED:
                    return self._states_logic.programmed_arm

        # Any transition coming from the password verification previous to the UserNumberConfig
        elif state == ControllerState.UserNumberPreConf:
            if payload == InputPayload.ESCAPE.value:
                return self._states_logic.configuration_clear
            elif self.handle_keyboard_numbers(payload, mode):
                return self._states_logic.user_number_conf_show_pass
            elif payload == InputPayload.ENTER.value:
                return self._states_logic.user_number_conf_check_def_pass

        # Any transition coming from the UserCodeConfiguration
        elif state == ControllerState.UserNumberConf:
            if self.handle_keyboard_numbers(payload, mode):
                return self._states_logic.user_number_conf_show_pass
            elif payload == InputPayload.ENTER.value and mode == InputMode.HOLD_3SEC:
                return self._states_logic.user_number_save

        # Any transition from Security code (password verification)
        elif state == ControllerState.SecurityNumberPreConf:
            if payload == InputPayload.ESCAPE.value:
                return self._states_logic.configuration_clear
            elif self.handle_keyboard_numbers(payload, mode):
                return self._states_logic.user_number_conf_show_pass
            elif payload == InputPayload.ENTER.value:
                return self._states_logic.security_number_conf_check_def_pass

        elif state == ControllerState.SecurityPhoneNumberConf:
            if payload == InputPayload.ESCAPE.value:
                return self._states_logic.configuration_clear
            elif self.handle_keyboard_numbers(payload, mode):
                return self._states_logic.security_number_conf_show_number
            elif payload == InputPayload.ENTER.value and mode == InputMode.HOLD_3SEC:
                return self._states_logic.security_number_save

        elif state == ControllerState.ArmingCodeConf:
            if payload == InputPayload.ESCAPE.value:
                return self._states_logic.configuration_clear
            elif self.handle_keyboard_numbers(payload, mode):
                return self._states_logic.arming_conf_show_mode
            elif payload == InputPayload.ENTER.value:
                return self._states_logic.arming_conf_set_mode

        elif state in [ControllerState.ArmingCodeConfMod0, ControllerState.ArmingCodeConfMod1]:
            if payload == InputPayload.ESCAPE.value:
                return self._states_logic.configuration_clear
            elif self.handle_keyboard_numbers(payload, mode):
                return self._states_logic.arming_conf_show_code
            elif payload == InputPayload.ENTER.value and mode == InputMode.HOLD_3SEC:
                return self._states_logic.arming_save_code

        elif state == ControllerState.ZoneProgramming:
            if payload == InputPayload.ESCAPE.value:
                return self._states_logic.configuration_clear
            elif self.handle_keyboard_numbers(payload, mode):
                return self._states_logic.programming_zones_show_numbers
            elif payload == InputPayload.ENTER.value:
                if mode == InputMode.HOLD_3SEC:
                    print('Sensors in Zone 1: ', self.__controller.Mode_1_Sensors_List)   ## print list of sensors to console
                    return self._states_logic.programming_zones_finish
                else:
                    return self._states_logic.programming_zones_save

        elif state in [ControllerState.ArmingMode_0, ControllerState.ArmingMode_1, ControllerState.Mode_0_Triggered, ControllerState.Mode_1_Triggered]:
            if self.handle_all_keyboard_chars(payload, mode):
                return self._states_logic.programmed_show_numbers
            elif payload == InputPayload.ESCAPE.value:
                return self._states_logic.configuration_clear
            elif payload == InputPayload.ENTER.value:
                if mode != InputMode.HOLD_3SEC:
                    return self._states_logic.programmed_arm

        # Panic buttons
        if payload == InputPayload.PANIC.value and mode == InputMode.HOLD_3SEC and  self.__controller.state == ControllerState.Programmed:
            return self._states_logic.trigger_panic

        if payload == InputPayload.FIRE.value and mode == InputMode.HOLD_3SEC  and  self.__controller.state == ControllerState.Programmed:
            return self._states_logic.trigger_fire

        return lambda: True
