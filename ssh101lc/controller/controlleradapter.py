#
# Software Verification and Validation course
# Master in Electronics Engineering - 2021
# Author: Luis G. Leon Vega <luis@luisleon.me>
#
import time
import re
import os

from ssh101lc.controller.controllerhandler import ControllerHandler
from ssh101lc.outputhandlers.outputenums import OutputType, OutputMode, OutputPayload, AlarmType
from ssh101lc.controller.controllerenums import ControllerState
from ssh101lc.inputhandlers.inputenums import InputMode, InputType, InputPayload
from ssh101lc.controller.statesadapter import StatesLogic, StatesTransition
from ssh101lc.logger.logger import get_logger

class Controller(ControllerHandler):

    def __init__(self, input_handlers={}, output_handlers={}):
        self.state = ControllerState.Unprogrammed
        self.configured = {}
        #self.debug = True
        self.debug = os.environ.get("DEBUG") == "1" 
        #if self.debug and False:
        if self.debug and False:
            self.configured = {
                "UserCode" : True,
                "SecurityNumber" : True,
                "ArmingCode0" : True,
                "ArmingCode1" : True
            }
        self.user_code = "0000"
        self.security_number = "00000000"
        self.arming_mode_0 = "000000"  # code for arming mode 0
        self.arming_mode_1 = "000000"  # code for armng mode 1
        self.Mode_0_Sensors = ""  # user input string listing sensors in Zone 0
        self.Mode_1_Sensors = ""  # user input string listing sensors in Zone 1
        self.Mode_0_Sensors_List = [i for i in range(0, 16)]  # list of sensors Zone 0 (from 0 to 15)
        self.Mode_1_Sensors_List = []  # list of sensors Zone 0 (from 0 to 9)
        self.logger = get_logger()
        self._states_logic = StatesLogic(self, self.logger)
        self._states_transition = StatesTransition(self, self.logger)

        self.SelectedMode = ""  # Mode 0 or Mode 1
        self.ModeSelectionDone = False
        self.NextStateBlocked = 0
        super().__init__(input_handlers, output_handlers)

    def stop(self):
        super().stop()
        self.Armed_LED_Off()
        self.Battery_LED_Off()
        self._states_logic.stop()

    def __del__(self):
        self.stop()

    def __on_input(self, message):
        if message["type"] == InputType.KEYBOARD:
            self.__on_button_pressed(message)
        elif message["type"] == InputType.SENSOR:
            self.__on_sensor_activated(message)
        elif message["type"] == InputType.BATTERY:
            self.__on_battery_level(message)

    def __on_sensor_activated(self, message):
        if self.debug:
            print("Sensor", message)
        self._states_transition.sensor_logic(message["payload"], message["mode"])


    def __on_battery_level(self, message):
        if self.debug:
            print("Battery", message)
        self._states_transition.battery_logic(message["payload"], message["mode"])

    def __on_button_pressed(self, message):
        if self.debug:
            print("Button", message)
            print("Current State: ", self.state)
        next = self._states_transition.next_states_logic(message["payload"], message["mode"])
        next()
        if self.debug:
            print("Current State: ", self.state)
        
    def init(self):
        """
        Initialises the objects
        """
        for handler in self.input_handlers:
            self.input_handlers[handler].install_callback(self.__on_input)

    def reset(self):
        """
        Restarts the controller
        """
        pass

    def start(self):
        super().start()
        self._states_logic.to_unprogrammed()
        self._states_logic.start()

    def clear_screen(self):
        show_message = {  # Clear the screen
            "type": OutputType.SCREEN,
            "payload": "",
            "mode": OutputMode.CLEAR
        }
        self.output_handlers['screen'].show(show_message)

    def show_screen(self, message):
        show_message = {
            "type": OutputType.SCREEN,
            "payload": message['payload'],
            "mode": OutputMode.SHOW
        }
        self.output_handlers['screen'].show(show_message)

    def show_Mode0_Indicator(self, message):
        show_message = {
            "type": OutputType.INDICATOR,
            "payload": OutputPayload.MODE_0,
            "mode": OutputMode.SHOW
        }
        self.output_handlers['screen'].show(show_message)

    def clear_Mode0_Indicator(self, message):
        show_message = {
            "type": OutputType.INDICATOR,
            "payload": OutputPayload.MODE_0,
            "mode": OutputMode.CLEAR
        }
        self.output_handlers['screen'].show(show_message)

    def show_Mode1_Indicator(self, message):
        show_message = {
            "type": OutputType.INDICATOR,
            "payload": OutputPayload.MODE_1,
            "mode": OutputMode.SHOW
        }
        self.output_handlers['screen'].show(show_message)

    def clear_Mode1_Indicator(self, message):
        show_message = {
            "type": OutputType.INDICATOR,
            "payload": OutputPayload.MODE_1,
            "mode": OutputMode.CLEAR
        }
        self.output_handlers['screen'].show(show_message)

    def show_Error_Indicator(self, message):
        show_message = {
            "type": OutputType.INDICATOR,
            "payload": OutputPayload.ERROR,
            "mode": OutputMode.SHOW
        }
        self.output_handlers['screen'].show(show_message)

    def clear_Error_Indicator(self, message):
        show_message = {
            "type": OutputType.INDICATOR,
            "payload": OutputPayload.ERROR,
            "mode": OutputMode.CLEAR
        }
        self.output_handlers['screen'].show(show_message)

    def show_Battery_Indicator(self):
        show_message = {
            "type": OutputType.INDICATOR,
            "payload": OutputPayload.BATTERY_LABEL,
            "mode": OutputMode.SHOW
        }
        self.output_handlers['screen'].show(show_message)

    def clear_Battery_Indicator(self):
        show_message = {
            "type": OutputType.INDICATOR,
            "payload": OutputPayload.BATTERY_LABEL,
            "mode": OutputMode.CLEAR
        }
        self.output_handlers['screen'].show(show_message)

    def Battery_LED_On(self):
        show_message = {
            "type": OutputType.INDICATOR,
            "payload": OutputPayload.BATTERY,
            "mode": OutputMode.SHOW
        }
        self.output_handlers['screen'].show(show_message)

    def Battery_LED_Off(self):
        show_message = {
            "type": OutputType.INDICATOR,
            "payload": OutputPayload.BATTERY,
            "mode": OutputMode.CLEAR
        }
        self.output_handlers['screen'].show(show_message)

    def Battery_LED_Blink(self):
        show_message = {
            "type": OutputType.INDICATOR,
            "payload": OutputPayload.BATTERY,
            "mode": OutputMode.BLINK
        }
        self.output_handlers['screen'].show(show_message)

    def Armed_LED_On(self):
        show_message = {
            "type": OutputType.INDICATOR,
            "payload": OutputPayload.ARMED,
            "mode": OutputMode.SHOW
        }
        self.output_handlers['screen'].show(show_message)

    def Armed_LED_Off(self):
        show_message = {
            "type": OutputType.INDICATOR,
            "payload": OutputPayload.ARMED,
            "mode": OutputMode.CLEAR
        }
        self.output_handlers['screen'].show(show_message)

    def Armed_LED_Blink(self):
        show_message = {
            "type": OutputType.INDICATOR,
            "payload": OutputPayload.ARMED,
            "mode": OutputMode.BLINK
        }
        self.output_handlers['screen'].show(show_message)

    def process_sensors_list(self, Mode_0_Sensors, Zone, Sensors_List):

        sensors_new_list = Sensors_List
       # sensors_new = []
        sensors_str = re.findall('..?', Mode_0_Sensors)
        if Zone == "00":
            for i in range(0, len(sensors_str)):
                if int(sensors_str[i]) <= 15:   # sensors in zone 0 in range 0 -15
                    if int(sensors_str[i]) in sensors_new_list:
                        sensors_new_list.pop(
                            sensors_new_list.index(int(sensors_str[i])))
                    else:
                        sensors_new_list.append(int(sensors_str[i]))
        else:
            for i in range(0, len(sensors_str)):
                if int(sensors_str[i]) <= 9:     # sensors in zone 1 in range 0 -9
                    if int(sensors_str[i]) in sensors_new_list:
                        sensors_new_list.pop(
                            sensors_new_list.index(int(sensors_str[i])))
                    else:
                        sensors_new_list.append(int(sensors_str[i]))
        return sensors_new_list

    def print_list_sensors(self, sensors_list):
        for x in range(len(sensors_list)):
            print(sensors_list[x])

    def trigger_alarm0(self, sensor):
        comms_message = {
            "type": OutputType.COMMS,
            "payload": {
                "number": self.security_number,
                "user_code": self.user_code,
                "alarm_type": AlarmType.THEFT,
                "sensor_code": sensor
            },
            "mode": None
        }
        horn_message = {
            "type": OutputType.HORN
        }
        if self.output_handlers.get('comms'):
            self.output_handlers['comms'].show(comms_message)
        if self.output_handlers.get('horn'):
            self.output_handlers['horn'].set_sound()
            self.output_handlers['horn'].show(horn_message)


    def trigger_panic_buttons(self, alarm_type):
        comms_message = {
            "type": OutputType.COMMS,
            "payload": {
                "number": self.security_number,
                "user_code": self.user_code,
                "alarm_type": alarm_type.value
            },
            "mode": None
        }
        horn_message = {
            "type": OutputType.HORN
        }
        if self.output_handlers.get('comms'):
            self.output_handlers['comms'].show(comms_message)
        if self.output_handlers.get('horn'):
            self.output_handlers['horn'].set_sound()
            self.output_handlers['horn'].show(horn_message)

    def do_beep(self):
        horn_message = {
            "type": OutputType.HORN
        }
        self.output_handlers['horn'].set_sound(type=1)
        self.output_handlers['horn'].show(horn_message)

    def stop_beep(self):
        self.output_handlers['horn'].stop()
