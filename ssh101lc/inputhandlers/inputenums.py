from enum import Enum


class InputType(Enum):
    KEYBOARD = 0
    SENSOR = 1
    BATTERY = 2


class InputMode(Enum):
    TYPED = 0
    HOLD_1SEC = 1
    HOLD_3SEC = 2


class SensorMode(Enum):
    OPEN = 0
    CLOSE = 1


class InputPayload(Enum):
    ZERO = "0"
    ONE = "1"
    TWO = "2"
    THREE = "3"
    FOUR = "4"
    FIVE = "5"
    SIX = "6"
    SEVEN = "7"
    EIGHT = "8"
    NINE = "9"
    ASTERISK = "10"
    HASH = "11"
    ESCAPE = "12"
    ENTER = "13"
    PANIC = "14"
    FIRE = "15"
    ENTER_ASTERISK = "16"
    ENTER_HASH = "17"
    ENTER_ESCAPE = "18"
