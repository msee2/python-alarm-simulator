
#
# Software Verification and Validation course
# Master in Electronics Engineering - 2021
# Author: Luis G. Leon Vega <luis@luisleon.me>
#

from ssh101lc.inputhandlers.inputhandler import InputHandler
from ssh101lc.inputhandlers.inputenums import InputType

class SensorAdapter(InputHandler):
    """
    Sensor class for triggering the sensors

    Methods
    ----------
    start()
        Abstract. Starts listening to new events
    stop()
        Abstract. Stops listening to events and destroy all the resources
    trigger(payload)
        Trigger the callback
    """

    def __init__(self, cli):
        """
        Constructs a new Sensor
        """
        self.__state = False
        self.__cli = cli
        super().__init__()

    def start(self):
        """
        Starts the sensor listener
        """
        self.__state = True
        self.__cli.attach_sensor_callback(self.trigger)
        self.__cli.start()

    def stop(self):
        """
        Stops the sensor listener
        """
        self.__state = False
        self.__cli.attach_sensor_callback(None)

    def trigger(self, payload):
        """
        Triggers the callback
        """
        if self.__state:
            sensor_msg = {
                "type": InputType.SENSOR,
                "payload": payload,
                "mode" : 0
            }
            super().trigger(sensor_msg)
