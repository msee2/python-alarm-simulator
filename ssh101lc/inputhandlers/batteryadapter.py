#
# Software Verification and Validation course
# Master in Electronics Engineering - 2021
# Author: Luis G. Leon Vega <luis@luisleon.me>
#

from threading import Thread, Lock
from time import sleep

from ssh101lc.inputhandlers.inputhandler import InputHandler
from ssh101lc.inputhandlers.inputenums import InputType, SensorMode


class BatteryAdapter(InputHandler):
    """
    Sensor class for triggering the sensors

    Methods
    ----------
    start()
        Abstract. Starts listening to new events
    stop()
        Abstract. Stops listening to events and destroy all the resources
    trigger(payload)
        Trigger the callback
    """

    def __init__(self, cli):
        """
        Constructs a new Sensor
        """
        self.__state = False
        self.__cli = cli
        self.__current_val = 12.0
        self.__old_val = 12.0
        self.__terminate = False
        self.__lock = Lock()
        self.__thread = None
        super().__init__()

    def start(self):
        """
        Starts the sensor listener
        """
        self.__state = True
        self.__cli.attach_battery_callback(self.trigger)
        self.__cli.start()
        with self.__lock:
            self.__terminate = False
        if not self.__thread:
            self.__thread = Thread(target=self.__worker)
            self.__thread.start()

    def __worker(self):
        terminate = False
        seconds = 0
        while not terminate:
            sleep(1) 
            seconds += 1

            with self.__lock:
                terminate = self.__terminate
                battery_msg = {
                    "type": InputType.BATTERY,
                    "payload": self.__old_val,
                    "mode" : None
                }
                super().trigger(battery_msg)
            if self.__state and seconds > 30: # 30 seconds to refresh the battery
                self.__old_val= self.__current_val
                seconds = 0

    def stop(self):
        """
        Stops the sensor listener
        """
        self.__state = False
        self.__cli.attach_battery_callback(None)
        with self.__lock:
            self.__terminate = True
        if self.__thread:
            self.__thread.join()
            self.__thread = None
        self.__cli.stop()

    def trigger(self, payload):
        """
        Triggers the callback
        """
        with self.__lock:
            self.__current_val = payload
        
    def __del__(self):
        self.stop()
