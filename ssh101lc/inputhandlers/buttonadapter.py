#
# Software Verification and Validation course
# Master in Electronics Engineering - 2021
# Author: Luis G. Leon Vega <luis@luisleon.me>
#

from ssh101lc.inputhandlers.inputhandler import InputHandler


class ButtonAdapter(InputHandler):
    """
    Sensor class for triggering the sensors

    Methods
    ----------
    start()
        Abstract. Starts listening to new events
    stop()
        Abstract. Stops listening to events and destroy all the resources
    trigger(payload)
        Trigger the callback
    """

    def __init__(self):
        """
        Constructs a new Sensor
        """
        self.__state = False
        super().__init__()

    def start(self):
        """
        Starts the sensor listener
        """
        self.__state = True

    def stop(self):
        """
        Stops the sensor listener
        """
        self.__state = False

    def trigger(self, payload):
        """
        Triggers the callback
        """
        if self.__state:
            super().trigger(payload)
