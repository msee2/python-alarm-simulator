#
# Software Verification and Validation course
# Master in Electronics Engineering - 2021
# Author: Luis G. Leon Vega <luis@luisleon.me>
#

import abc


class InputHandler(abc.ABC):
    """
    Interface class for abstracting and standardising the input modules

    Methods
    ----------
    start()
        Abstract. Starts listening to new events
    stop()
        Abstract. Stops listening to events and destroy all the resources
    install_callback(callback)
        Installs the callback
    """

    def __init__(self):
        """
        Constructs a new Input Handler
        """
        self.__callback = None

    @abc.abstractmethod
    def start(self):
        """
        Starts the input listener. This will deploy threads and other
        structures required for polling or handling the events.
        """
        pass

    @abc.abstractmethod
    def stop(self):
        """
        Stops the input listener. This will destroy threads and other
        structures required for polling or handling the events.
        """
        pass

    def __del__(self):
        """
        Destructor
        """
        self.stop()

    def trigger(self, payload):
        """
        Triggers the callback
        """
        self.__callback(payload)

    def install_callback(self, callback):
        """
        Installs the callback in case of having an input event

        Parameters
        ----------
        callback : function
            Callback to install
        """
        self.__callback = callback
