from tkinter import Tk
import threading
import time

from ssh101lc.inputhandlers.buttonadapter import ButtonAdapter
from ssh101lc.inputhandlers.sensoradapter import SensorAdapter
from ssh101lc.inputhandlers.batteryadapter import BatteryAdapter
from ssh101lc.controller.controlleradapter import Controller
from ssh101lc.gui.gui import GUI
from ssh101lc.outputhandlers.ScreenIndicatorsAdapter import ScreenIndicatorsAdapter
from ssh101lc.outputhandlers.commsadapter import CommsAdapter
from ssh101lc.outputhandlers.hornadapter import HornAdapter
from ssh101lc.cli.cli import CLIConnector

window = None
control = None

thread = None
finish = False

def __update_gui(gui):
    global finish

    if gui:
        while not finish:
            gui.Update_Gui()
            time.sleep(1)
        gui.stop()

def close():
    global window
    global control
    global finish
    global thread

    finish = True
    if thread:
        thread.join()
        thread = None
    print("Please, press q to close the CLI as well")
    control.stop()
    window.destroy()
    exit(0)

    
def main():
    global window
    global control
    global finish
    global thread

    cli = CLIConnector()
    sensor = SensorAdapter(cli)
    battery = BatteryAdapter(cli)
    button = ButtonAdapter()
    input_handlers = {
        "sensor": sensor,
        "button": button,
        "battery": battery
    }

    comms = CommsAdapter()
    horn = HornAdapter()
    Screen_Indicators = ScreenIndicatorsAdapter()
    output_handlers = {
        "screen": Screen_Indicators,
        "comms": comms,
        "horn": horn
    }

    # GUI
    window = Tk()
    gui = GUI(window, sensor , button, Screen_Indicators)

    control = Controller(input_handlers, output_handlers)
    control.init() # Install callbacks
    control.start() # Start instances
    window.protocol("WM_DELETE_WINDOW", close)

    if not thread:
        thread = threading.Thread(target=__update_gui, args=(gui,))
        thread.start()
    
    window.mainloop()

if __name__ == "__main__":
    main()
