import logging


def get_logger(name="Simulator"):
    # create logger
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter(
        '[%(created)f] %(filename)s:%(funcName)s %(levelname)s  %(message)s')
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)

    return logger
