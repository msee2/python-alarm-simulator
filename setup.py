# Copyright 2020 - NanoSciTracker
# Author: Luis G. Leon Vega <luis@luisleon.me>

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ssh101lc",
    version="0.1.0",
    author="Luis G. Leon-Vega, Christian Jimenez",
    author_email="luis@luisleon.me, cjimenez@teradyne.com",
    description="Sistema de Alarma SSH-101",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/msee2/python-alarm-simulator",
    packages=setuptools.find_packages(
        exclude=[
            "*.tests",
            "*.tests.*",
            "tests.*",
            "tests"]),
    scripts=[
        'bin/ssh101lc',
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        "tk>= 0.1",
        "pydub>= 0.25",
        "simpleaudio>=1.0.4"
    ],
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    python_requires='>=3.8',
)
