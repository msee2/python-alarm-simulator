# Sistema de Alarma SSH-101

This repository contains the project for the SW Verification & Validation

## Installation - Linux

Install the dependencies:

* Dependencies...

You can install them by:

```bash
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get update
sudo apt-get python3 python3-pip
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.8 10
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.8 10
python -m pip install pip
```

> It's better if you set an isolated environment if you don't want to override
your configurations. You can also use conda for the environment:

```bash
conda create -n "python3.8" python=3.8
conda activate python3.8
```

You can install the project:

```bash
pip3 install -e .
```

## Usage

```bash
python3 -m ssh101lc
# or
ssh101lc
```

For debugging purposes, you can set the `DEBUG=1` environment variable for
looking at state transitions, messages and other useful messages. To do so,
in Linux, you can do:

```bash
DEBUG=1 ssh101lc
```

## MISC

Version: 0.1.0

Luis G. Leon-Vega
Christian Jimenez
