#!/usr/bin/env python3

from ssh101lc.inputhandlers.sensor import SensorAdapter


def display_message(payload):
    print("Typed", payload)


if __name__ == "__main__":
    sensor = SensorAdapter()
    sensor.install_callback(display_message)
    sensor.start()

    while(True):
        x = input("Type command: ")
        sensor.trigger(x)
